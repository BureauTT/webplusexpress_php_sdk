<?php



class responseAPI{
    
    private $_sucess;
    private $_error_code;
    private $_error_message;
    private $_operation;
    private $_service_result;
    private $_HTTP_Response_Code;
    private $_phone;
    private $_reqid;
    
    public function responseAPI(){
       $this->_sucess=false;
       $this->_error_code=null;
       $this->_error_message=null;
       $this->_operation=null;
       $this->_service_result=null;   
       $this->_HTTP_Response_Code="401";
    }
    
    public function is_success(){
        return $this->_sucess;
    }
    
    public function set_success($success){
        $this->_sucess=$success;
    }
    
    public function get_error_code(){
        return $this->_error_code;
    }
    
    public function set_error_code($error_code){
      $this->_error_code=$error_code;
    }
    
    public function get_error_message(){
        return $this->_error_message;
    }
    
    public function set_error_message($error_message){
      $this->_error_message=$error_message;
    }
    
    public function get_operation(){
        return $this->_operation;
    }
    
    
    public function set_operation($operation){
        $this->_operation=$operation;
    }
    
    public function get_service_result(){
        return $this->_service_result;
    }
    
    public function set_service_result($service_result){
        $this->_service_result=$service_result;
    }
    
    
    public function set_HTTP_Response_Code($http_code){
     $this->_HTTP_Response_Code=$http_code;   
    }
    
    public function get_HTTP_Response_Code(){
       return $this->_HTTP_Response_Code; 
    }
    
    public function get_phone(){
        return $this->_phone;
    }
    
    
    public function set_phone($phone){
        $this->_phone=$phone;
    }
    
    public function get_reqid(){
        return $this->_reqid;
    }
    
    public function set_reqid($reqid){
        $this->_reqid=$reqid;
    }
    
}
