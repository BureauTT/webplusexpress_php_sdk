<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Config{
    
    
   private $_wpe_client_id;
   private $_wpe_client_secret;
   private $_wpe_auth_url;
   private $_wpe_token_url;
   private $_wpe_api_url;
 
   
   
   function Config(){
    $config= parse_ini_file("./CONFIG/config.ini",true);
    $this->_wpe_client_id=$config['oauth2_credentials']['wpe_client_id'];
    $this->_wpe_client_secret=$config['oauth2_credentials']['wpe_client_secret'];
    $this->_wpe_token_url=$config['oauth2_urls']['wpe_token_url'];
    $this->_wpe_api_url=$config['api_url']['wpe_api_url'];
    }
    
    
    
  function _getclient_id(){     
      return $this->_wpe_client_id;
  }
  
  function _getclient_secret(){
      return $this->_wpe_client_secret;
  }
  
  
  
  function _geturl_token(){
      return $this->_wpe_token_url;
  }
      
    
  function _geturl_api(){
      return $this->_wpe_api_url;
  } 
  
  
    
    
    
    
    
    
}
