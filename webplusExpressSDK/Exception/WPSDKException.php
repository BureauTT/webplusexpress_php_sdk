<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class WPSDKException extends Exception{
    
    
    const INTERNAL_SERVER_ErrorCode=1007;
    
    const ACCESS_TOKEN_ERROR_CODE=1020;
    
    const CURL_REQUEST_ERROR_CODE=1021;
    
    const EXTRA_PARAMS_REQUIRED_CODE=1022;
    
    const ACCESS_TOKEN_REQUIRED_CODE=1023;
    
    const API_RESPONSE_ERROR_CODE=1024;
    
    
    const EXTRA_PARAMS_REQUIRED_MESSAGE="Extra params username and password required for grant type password";
    const ACCESS_TOKEN_REQUIRED_MESSAGE="Access token required for API execute";
    
    
    public function __construct ($code,$message) {
       
       $this->code=$code;
       $this->message=$message;
      
    }
    
    public function errorMessage(){       
        return array('error_code'=>  $this->code,
                     'error_message'=> $this->message);
    }
    
    
    
}
