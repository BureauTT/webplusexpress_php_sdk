<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once "./UTIL/Config.php";

class webplusexpress{
    
  private $_config;  
  private $_access_token;
  private $_refresh_token;
    
  function webplusexpress(){
    try{
    $this->_config=(new Config()); 
    }catch(Exception $e){
    $e->getMessage();  
    }
  } 
    
  
  /**
   * 
   * @param type $grant_type
   * @param type $params_extra
   * @return type
   */
  public function initAcessToken($grant_type,
                                 $params_extra=null,
                                 $scopes=null){
               
    //var_dump($this->_getTokenRequestPramsFromGrant_Type($grant_type, $params_extra));
      
    try {
    $fp = fopen('errorlog.txt', 'w');
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $this->_config->_geturl_token(),
    CURLOPT_USERAGENT => 'webplusexpressSDK agent',
    CURLOPT_POST => 1,
    CURLOPT_SSL_VERIFYPEER=>FALSE,    
    CURLOPT_STDERR=>$fp,
    CURLOPT_RETURNTRANSFER => TRUE,    
    CURLOPT_VERBOSE => TRUE,
    CURLOPT_POSTFIELDS => $this->_getTokenRequestPramsFromGrant_Type($grant_type, $params_extra,$scopes)
    ));    
    
    // Send the request & save response to $resp    
    $respJson=json_decode(curl_exec($curl), true);
       
    if(!curl_exec($curl)){
    throw new WPSDKException( WPSDKException::CURL_REQUEST_ERROR_CODE,
            'Error CURL: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
    }
    // Close request to clear up some resources
    curl_close($curl);      
    if(!isset($respJson['access_token'])){
     throw new WPSDKException( WPSDKException::ACCESS_TOKEN_ERROR_CODE,
     $respJson['error_description']);
    }else{ 
     $this->_access_token=$respJson['access_token'];
     return $respJson['access_token'];
    }
    //return $respJson["access_token"];
    }catch(Exception $e){         
     throw new WPSDKException(
             WPSDKException::INTERNAL_SERVER_ErrorCode,
             $e->getMessage());
     return null;
    }
  } 
  
  
  /**
   * 
   * @param type $grant_type
   * @param type $params_extra
   */
  private function _getTokenRequestPramsFromGrant_Type($grant_type,
                                                      $params_extra,
                                                      $scopes){ 
        switch($grant_type){
          case grant_type::client_credentials:
          return array("client_id" => $this->_config->_getclient_id(),
                       "client_secret"=>$this->_config->_getclient_secret(),
                       "grant_type"=>$grant_type,
                       "scope"=>$scopes);
          case grant_type::password:
          if(!isset($params_extra['username']) ||
             !isset($params_extra['password'])){
            throw new WPSDKException(WPSDKException::EXTRA_PARAMS_REQUIRED_CODE,
            WPSDKException::EXTRA_PARAMS_REQUIRED_MESSAGE);  
          }          
          return array("client_id" => $this->_config->_getclient_id(),
                       "username"=>$params_extra['username'],
                       "client_secret"=>$this->_config->_getclient_secret(),
                       "password"=>$params_extra['password'],
                       "grant_type"=>$grant_type,
                       "scope"=>$scopes); 
          
        }
  return null;    
  }
 
  /**
   * 
   * @param type $access_token
   * @param type $a
   * @param type $short_name
   * @param type $p
   * @param type $userid
   * @param type $md5Password
   * @param type $phone
   * @param type $reqid
   * @return null|\responseAPI
   * @throws WPSDKException
   */
  public function executeAPI($access_token, 
                             $a = "webdo",
                             $short_name, 
                             $p, 
                             $userid, 
                             $md5Password, 
                             $phone="",
                             $reqid=0
                             ){
    try {        
    if($this->_access_token==null){
      throw new WPSDKException(WPSDKException::ACCESS_TOKEN_REQUIRED_CODE,
      WPSDKException::ACCESS_TOKEN_REQUIRED_MESSAGE);  
    }
    
    $str="a=". $a."&userid=".$userid."&password=".$md5Password."&phone=".$phone."&reqid=".$reqid."&p=".urlencode($p);
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $this->_config->_geturl_api()."/".$short_name."?".$str,
    CURLOPT_USERAGENT => 'webplusexpressSDK agent',
    CURLOPT_HTTPHEADER=>array('Authorization:Bearer '.$access_token),
    CURLOPT_SSL_VERIFYPEER=>FALSE   
    ));        
    
    $result=curl_exec($curl);
    //var_dump( $result);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if(!$result){
    throw new WPSDKException( WPSDKException::CURL_REQUEST_ERROR_CODE,
            'Error CURL: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
    }
    $respJson=json_decode($result, true); 
    curl_close($curl);   
    
    
    $responseAPI=new responseAPI();
    $responseAPI->set_HTTP_Response_Code($httpcode);
    $responseAPI->set_success($respJson['success']);    
    if(isset($respJson['phone']) && !empty($respJson['phone'])){
     $responseAPI->set_phone($respJson['phone']);
    }    
    if(isset($respJson['reqid']) && !empty($respJson['reqid'])){
     $responseAPI->set_reqid($respJson['reqid']);
    }    
    if($responseAPI->get_HTTP_Response_Code()=="200"){        
     $responseAPI->set_operation($respJson['operation']) ;  
     $responseAPI->set_service_result($respJson['result']);
    }else{
     $responseAPI->set_error_code($respJson['error']['Code']);
     $responseAPI->set_error_message($respJson['error']['Message']);
    }
    
    return $responseAPI;        
    }catch(Exception $e){         
     throw new WPSDKException(
             WPSDKException::INTERNAL_SERVER_ErrorCode,
             $e->getMessage());
    }
  } 
    
 /**
  * 
  * @return type
  */   
 public function _get_access_token(){
     return $this->_access_token;
 }
    
    
    
}



