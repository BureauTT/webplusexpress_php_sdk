# README #

Webplus express PHP SDK is a small framework that allows you to call Web+ Express API methods and execute the web + services.
To use the SDK, download the code from repository and integrate it in your library project.
------
Webplus express PHP SDK est un petit framework qui permet d'utiliser l'API web plus express pour éxécuter les services web+.
Pour utiliser le SDK, téléchargez le dépot et integrez le dans votre librairie du projet.

### Api web+ Express  ###
* [Api web+ Express](https://www.webplusexpress.com/dokuwiki/api_web_plus_express)

### How do I get set up? ###

* Download and copy sdk in your library project.
* Get yours oauth2 client_id and client_secret codes from https://www.webplusexpress.com.
* If you use password grant_types you must have also wpe username and password crypted in md5.
* Edit CONFIG/config.ini [oauth2_credentials]  set wpe_client_id and wpe_client_secret.
* Use test.php file in the repository to run webdo, smsdo and test calls.


### Who do I talk to? ###

* webmaster@webplusexpress.com
* contact@webplusexpress.com